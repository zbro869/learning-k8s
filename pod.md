## Running Pods

```
learning-k8s git:(master) ✗ kubectl apply -f kuard-pod.yaml 
pod/kuard created

learning-k8s git:(master) ✗ kubectl get pods
NAME    READY   STATUS    RESTARTS   AGE
kuard   1/1     Running   0          12s
```

* The pod manifest will be submitted to Kubernetes API Server.
* The Kubernetes system will then schedule that Pod to run on a healthy node in the cluster.

## Describe the pod

```
learning-k8s git:(master) ✗ kubectl describe pods kuard 
Name:         kuard
Namespace:    default
Priority:     0
Node:         lke11724-14579-5f8bc5a50418/192.168.150.208
Start Time:   Sat, 24 Oct 2020 21:06:18 +0800
Labels:       <none>
Annotations:  cni.projectcalico.org/podIP: 10.2.1.2/32
Status:       Running
IP:           10.2.1.2
IPs:
  IP:  10.2.1.2
Containers:
  kuard:
    Container ID:   docker://f5a984fdea221c15578e3a8fe4028d1011bcbb0b76984d03db770b402667b726
    Image:          gcr.io/kuar-demo/kuard-amd64:blue
    Image ID:       docker-pullable://gcr.io/kuar-demo/kuard-amd64@sha256:1ecc9fb2c871302fdb57a25e0c076311b7b352b0a9246d442940ca8fb4efe229
    Port:           8080/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Sat, 24 Oct 2020 21:06:23 +0800
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-7vb9t (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  default-token-7vb9t:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-7vb9t
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                 node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age   From               Message
  ----    ------     ----  ----               -------
  Normal  Scheduled  11m   default-scheduler  Successfully assigned default/kuard to lke11724-14579-5f8bc5a50418
  Normal  Pulling    11m   kubelet            Pulling image "gcr.io/kuar-demo/kuard-amd64:blue"
  Normal  Pulled     11m   kubelet            Successfully pulled image "gcr.io/kuar-demo/kuard-amd64:blue"
  Normal  Created    11m   kubelet            Created container kuard
  Normal  Started    11m   kubelet            Started container kuard
  ```
  
  ## Deleting pod
  
  ```
  
  kubectl delete pods/kuard ## delete by name
  
  kubectl delete -f kuard-pod.yaml  ## delete by file
  ```
  
  ## Accessing pod
  
  - port forward
  
```
kubectl port-forward kuard 8080:8080
```
  - get logs of pod

```
kubectl logs kuard
```

## Run commands in pod

```
 kubectl exec kuard -- date
Sat Oct 24 13:48:32 UTC 2020

kubectl exec -it kuard -- ash // run interactive commands in pod
```

## Copy files to pod

```
kubectl cp README.md kuard:/readme.md
tar: can't open 'readme.md': Permission denied
command terminated with exit code 1
```

## Liveness Probe

```
      livenessProbe:
        httpGet:
          path: /healthy
          port: 8080
        initialDelaySeconds: 5
        timeoutSeconds: 1
        periodSeconds: 10
        failureThreshold: 3
```
- initialDelaySeconds
  after all containers created in the pod, 5s seconds later, the healthy endpoint will be called.

- 


## Readiness Probe

readiness probe describe when a container is ready to serve user requests.

containers failed readiness checks, are reoved from service load balancer.

